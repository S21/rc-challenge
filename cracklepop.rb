# Write a program that prints out the numbers 1 to 100 (inclusive).
# If the number is divisible by 3, print Crackle instead of the number.
# If it is divisible by both 3 and 5, print CracklePop. You can use any language.

my_number = 30

if my_number <= 100
  puts my_number
elsif my_number % 3 == 0 && my_number % 5 == 0
  puts 'CracklePop'
else
  puts 'Crackle'
end
